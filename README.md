# Intern Challenge #

### Description ###

* Create a Vuejs App for displaying various Climate related data with authentication/authorization and with a discussion board.

### Climate API ###

* [Open Weather ](http://openweathermap.org/api)

### Data To Be Displayed ###

* Weather
* Forecast
* Pollution Data, UV Index

### Functionality ###

* Use [Firebase](https://firebase.google.com/) for saving data
* Login - Any social media login or user/password
* Discussion Board 
	* CRUD  ex : Use can create, read, update, delete a post
	* Others can comment on it 